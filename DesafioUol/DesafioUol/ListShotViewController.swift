//
//  ListShotViewController.swift
//  DesafioUol
//
//  Created by Michel de Sousa Carvalho on 18/09/17.
//  Copyright © 2017 Michel de Sousa Carvalho. All rights reserved.
//

import UIKit

class ListShotViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var elements: [Shot] = []
    var pageNumber: Int = 1
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.black
        
        return refreshControl
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.addSubview(self.refreshControl)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.pageNumber = 1
        
        self.getShots(page: self.pageNumber)
        
        self.tableView.estimatedRowHeight = 211;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Shots"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    
    func getShots(page: Int) {
        Request().load(resource: Resource<[Shot]>(url: URL.initWithPage(numberPage: String(page), perPage: Constants.API.PERPAGE), parseJSON:{ (json) -> [Shot]? in
            guard let dictionaries = json as? [JSONDictionary] else { return nil }
            return dictionaries.flatMap(Shot.init)
        })) { (result) in
            if let result = result {
                self.elements.append(contentsOf: result)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.elements.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! ShotTableViewCell
        
        let element = self.elements[indexPath.row]
        
        
        cell.titleImage.text  = element.titleImage
        
        
        let imageCache = ImageCache.sharedInstance()!
        imageCache.imageRequest.fetchImage(withUrl: element.urlImageNormal, andNameImage: String(element.id)) { (image) in
            if let image = image {
                UIView.animate(withDuration: 0.3, animations: {
                    cell.show(image: image, imageView: cell.imageShot)
                })
            }
        }
        
        imageCache.imageRequest.fetchImage(withUrl: element.avatarURL, andNameImage: element.name+"_avatar") { (image) in
            if let image = image {
                UIView.animate(withDuration: 0.3, animations: {
                    cell.show(image: image, imageView: cell.avatarImage)
                    UIImageView.circleImage(with: cell.avatarImage)
                })
                
            }
        }
        
        
        
        return cell
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.elements.count-15 {
            self.pageNumber =  self.pageNumber+1;
            self.getShots(page: self.pageNumber)
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? ShotTableViewCell {
            let imageCache = ImageCache.sharedInstance()!
            let element = self.elements[indexPath.row]
            imageCache.imageRequest .cancelImageDownload(withURL: element.urlImageNormal) {
                cell.hide(imageView: cell.imageShot)
            }
            imageCache.imageRequest .cancelImageDownload(withURL: element.avatarURL) {
                cell.hide(imageView: cell.avatarImage)
            }
            
            cell.transform = CGAffineTransform.identity
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.elements.indices.contains(indexPath.row) {
            self.performSegue(withIdentifier: Constants.Segue.segueDetail, sender:  self.elements[indexPath.row])
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    // MARK: Refresh Control
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        Request().load(resource: Shot.firstsShots) { result in
            if let result = result {
                self.elements = result
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
        }
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segue.segueDetail {
            if let detailVC = segue.destination as? DetailViewController {
                if let shot = sender as? Shot {
                    detailVC.shotSelected = shot
                }
            }
        }
    }
    
    
    
}


