//
//  ShotTableViewCell.swift
//  DesafioUol
//
//  Created by Michel de Sousa Carvalho on 18/09/17.
//  Copyright © 2017 Michel de Sousa Carvalho. All rights reserved.
//

import UIKit

class ShotTableViewCell: UITableViewCell {

    @IBOutlet weak var imageShot: UIImageView!
    @IBOutlet weak var titleImage: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func show(image: UIImage, imageView: UIImageView) {
        imageView.image = image
        imageView.alpha = 1
    }

    func hide(imageView: UIImageView) {
        imageView.image = nil
        imageView.alpha = 0
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
