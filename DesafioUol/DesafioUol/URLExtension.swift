//
//  URLExtension.swift
//  DesafioUol
//
//  Created by Michel de Sousa Carvalho on 20/09/17.
//  Copyright © 2017 Michel de Sousa Carvalho. All rights reserved.
//

import Foundation


extension URL {
    
    static func initWithPage(numberPage: String, perPage: String) -> URL {
        var components = URLComponents.init(string: "shots/")
        let param1 = URLQueryItem.init(name: "access_token", value: Constants.API.CLIENTACCESSTOKEN)
        let param2 = URLQueryItem.init(name: "page", value: numberPage)
        let param3 = URLQueryItem.init(name: "per_page", value: perPage)
        
        components?.queryItems = [param1,param2,param3]
        let url = Constants.API.ENDPOINT + (components?.url?.absoluteString)!
        return URL.init(string:url)!
    }
    
}
