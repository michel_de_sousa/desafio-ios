//
//  ImageExtension.swift
//  DesafioUol
//
//  Created by Michel de Sousa Carvalho on 19/09/17.
//  Copyright © 2017 Michel de Sousa Carvalho. All rights reserved.
//

import Foundation


extension UIImageView {
    
    class func circleImage(with imageView: UIImageView) {
        imageView.layer.cornerRadius = imageView.frame.size.width/2;
        imageView.clipsToBounds = true;
    }

    
    
    
    
}
