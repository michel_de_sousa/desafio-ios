//
//  Constants.swift
//  DesafioUol
//
//  Created by Michel de Sousa Carvalho on 17/09/17.
//  Copyright © 2017 Michel de Sousa Carvalho. All rights reserved.
//

import Foundation



struct Constants {
    struct  API {
        static let CLIENTACCESSTOKEN = "8950cd199151ac0bf03a5266878a4853e0251a1ee0923e7b3663543820bf76d3"
        static let ENDPOINT = "https://api.dribbble.com/v1/"
        static let PERPAGE = "30"
        
        
        static let GETFIRSTSHOTS = URL(string: "https://api.dribbble.com/v1/shots/?access_token=8950cd199151ac0bf03a5266878a4853e0251a1ee0923e7b3663543820bf76d3&per_page=30")!
    }
    
    struct Segue {
        static let segueDetail = "segueDetail"
    }
    
}
