//
//  Request.swift
//  DesafioUol
//
//  Created by Michel de Sousa Carvalho on 17/09/17.
//  Copyright © 2017 Michel de Sousa Carvalho. All rights reserved.
//

import Foundation

class Request {
    func load<A>(resource:Resource<A>, completion: @escaping (A?) -> Void) {
        URLSession.shared.dataTask(with: resource.url as URL) { data, resp, _ in
// TODO: Pegar link do rel = next para detectar qndo chegar na ultima pagina
//            if let httpResponse = resp as? HTTPURLResponse {
//                let link = httpResponse.allHeaderFields["Link"] as? String
//                if let link = link {
//                }
//            }
            
            guard let data = data else {
                completion(nil)
                return
            }
            completion(resource.parse(data))
            }.resume()
    }
}

