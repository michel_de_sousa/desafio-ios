//
//  DetailViewController.swift
//  DesafioUol
//
//  Created by Michel de Sousa Carvalho on 21/09/17.
//  Copyright © 2017 Michel de Sousa Carvalho. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var shotImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    var shotSelected: Shot? = nil
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if let shot = self.shotSelected {
            self.name.text = shot.name
            self.detailLabel.text = shot.descriptionImg.html2String
            self.navigationItem.title = shot.titleImage
            
            let imageCache = ImageCache.sharedInstance()!
            imageCache.imageRequest.fetchImage(withUrl: shot.urlImageNormal, andNameImage: String(shot.id)) { (image) in
                if let image = image {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.shotImage.image = image;
                    })
                }
            }
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8), options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
