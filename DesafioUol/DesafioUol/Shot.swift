//
//  Shot.swift
//  DesafioUol
//
//  Created by Michel de Sousa Carvalho on 17/09/17.
//  Copyright © 2017 Michel de Sousa Carvalho. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String: Any]


struct Shot {
    let id: Int
    let name: String
    let avatarURL: String
    let titleImage: String
    let urlImageTeaser: String
    let urlImageNormal: String
    let descriptionImg: String
    let likesCount: Int
    let viewsCount: Int
    let commentsCount: Int
}


extension Shot {
    init?(dictionary: JSONDictionary) {
        
        self.id = dictionary["id"] as? Int ?? 0
        self.descriptionImg = dictionary["description"] as? String ?? ""
        
        let dictUser = dictionary["user"] as? JSONDictionary ?? [:]
        self.name = dictUser["name"] as? String ?? ""
        self.avatarURL = dictUser["avatar_url"] as? String  ?? ""
        
        
        let dictImg = dictionary["images"] as? JSONDictionary ?? [:]
        self.urlImageNormal = dictImg["normal"] as? String ?? ""
        self.urlImageTeaser = dictImg["teaser"] as? String ?? ""
        
        self.titleImage = dictionary["title"] as? String ?? ""
        self.likesCount = dictionary["likes_count"] as? Int ?? 0
        self.viewsCount = dictionary["views_count"] as? Int ?? 0
        self.commentsCount = dictionary["comments_count"] as? Int ?? 0
        
        
    }
    
    static let firstsShots = Resource<[Shot]>(url: Constants.API.GETFIRSTSHOTS, parseJSON: { json in
        guard let dictionaries = json as? [JSONDictionary] else { return nil }
        return dictionaries.flatMap(Shot.init)
    })
    
}
